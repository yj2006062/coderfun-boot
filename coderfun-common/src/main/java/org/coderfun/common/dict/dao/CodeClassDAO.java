package org.coderfun.common.dict.dao;

import org.coderfun.common.dict.entity.CodeClass;

import klg.common.dataaccess.BaseRepository;

public interface CodeClassDAO extends BaseRepository<CodeClass, Long> {

}
